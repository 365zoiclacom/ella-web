<?php  
 
require("phpmailer/class.phpmailer.php"); // path to the PHPMailer class
 
$mail = new PHPMailer();  
 
$mail->IsSMTP();  // telling the class to use SMTP
$mail->Mailer = "smtp";
$mail->Host = "ssl://smtp.gmail.com";
$mail->Port = 465;
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->Username = "admin@theellagroup.org"; // SMTP username
$mail->Password = "L12345thu"; // SMTP password 
 
$mail->From     = "admin@theellagroup.org";
$mail->FromName = "Guest Book";
$mail->AddAddress("res@theellagroup.org");

$field_room = $_POST['room_type'];
$field_check_in = $_POST['check_in'];
$field_check_out = $_POST['check_out'];
$field_name= $_POST['full_name'];
$field_email = $_POST['booking_email'];
$field_message = $_POST['ella_message'];  
 
$mail->Subject  = 'BOOKING '.$field_room;
$mail->Body = 'Room Type: '.$field_room."\n";
$mail->Body .= 'Check-in Date: '.$field_check_in."\n";
$mail->Body .= 'Check-out Date: '.$field_check_out."\n";
$mail->Body .= 'Name: '.$field_name."\n";
$mail->Body .= 'E-mail: '.$field_email."\n";
$mail->Body .= 'Message: '.$field_message;

$mail->WordWrap = 100;  
 
if($mail->Send()) { ?>
	<script language="javascript" type="text/javascript">
		alert('Thank you for the message. We will contact you shortly.');
		window.location = 'booking.html';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		alert('Message failed. Please, send an email to res@theellagroup.org');
		window.location = 'booking.html';
	</script>
<?php
}
?>