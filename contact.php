<?php  
 
require("phpmailer/class.phpmailer.php"); // path to the PHPMailer class
 
$mail = new PHPMailer();  
 
$mail->IsSMTP();  // telling the class to use SMTP
$mail->Mailer = "smtp";
$mail->Host = "ssl://smtp.gmail.com";
$mail->Port = 465;
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->Username = "admin@theellagroup.org"; // SMTP username
$mail->Password = "L12345thu"; // SMTP password 
 
$mail->From     = "admin@theellagroup.org";
$mail->FromName = "Guest Contact";
$mail->AddAddress("info@theellagroup.org");

$field_name = $_POST['contact_name'];
$field_email = $_POST['contact_email'];
$field_phone = $_POST['contact_phone'];
$field_message = $_POST['contact_message']; 
 
$mail->Subject  = 'Contact to Ella: '.$field_name;
$mail->Body .= 'Name: '.$field_name."\n";
$mail->Body .= 'E-mail: '.$field_email."\n";
$mail->Body .= 'Phone: '.$field_phone."\n";
$mail->Body .= 'Message: '.$field_message;

$mail->WordWrap = 100;  
 
if($mail->Send()) { ?>
	<script language="javascript" type="text/javascript">
		alert('Thank you for the message. We will contact you shortly.');
		window.location = 'booking.html';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		alert('Message failed. Please, send an email to res@theellagroup.org');
		window.location = 'booking.html';
	</script>
<?php
}
?>